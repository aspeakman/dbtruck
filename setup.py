# python setup.py bdist_wheel sdist --formats=gztar,zip
try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup
try: # for pip >= 20
    from pip._internal.req.req_file import parse_requirements
    from pip._internal.network.session import PipSession
except ImportError:
    try: # for pip >= 10
        from pip._internal.req import parse_requirements
        from pip._internal.download import PipSession
    except ImportError: # for pip <= 9.0.3
        from pip.req import parse_requirements
        from pip.download import PipSession
from version import get_version

package = 'dbtruck' # path on repository and package name
name = 'DBTruck' # full name on repository
details = 'Relaxed storage and retrieval from common databases'

required = [i.strip() for i in open("requirements.txt").readlines()]

# parse_requirements() returns generator of InstallRequirement objects
install_reqs = parse_requirements("requirements.txt", session=PipSession())

# requirements list eg. ['django==1.5.1', 'mezzanine==1.4.6']
try:
    requirements = [str(ir.req) for ir in install_reqs]
except (AttributeError):
    requirements = [str(ir.requirement) for ir in install_reqs]

try:
   import pypandoc
   description = pypandoc.convert('README.md', 'rst')
except (IOError, ImportError):
   description = open('README.md').read()

setup(
    name=name, 
    version=get_version(),
    author='Andrew Speakman',
    author_email='andrew@speakman.org.uk',
    packages=[package],
    include_package_data=True, # includes everything else specified in MANIFEST.in
    #scripts=['dddd'],
    url='https://bitbucket.org/aspeakman/' + package + '/',
    license='LICENSE.txt',
    python_requires='>=2.7',
    description=details,
    long_description=description,
    install_requires=requirements,
    classifiers=[
        "Development Status :: 4 - Beta", 
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
    ],
)

