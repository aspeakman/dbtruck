
import sqlite3

mem_db_conn = sqlite3.connect(":memory:")

valstr = 'xyx'
valuni = u'abc'

print(type(valstr), type(valuni))

sql = 'create table test (col1 text, col2 text)' 
mem_db_conn.execute(sql)

params = [ valstr, valuni ]
sql = 'insert into test (col1, col2) values (?, ?);'
mem_db_conn.execute(sql, params)

sql = 'select * from test;'
res = mem_db_conn.execute(sql).fetchall()
for r in res:
    print(r)
    print(type(r[0]), type(r[1]))

