# uploads a batch of records to a Postgres database
from __future__ import absolute_import, division, print_function
import time
from datetime import datetime, timedelta
import json
import argparse
import os

from planutils import classify, postprocess
from dbtruck.dbtruck import DBTruck, iquote, nquote
from sareas import SCRAPER_AREAS

#Note ambiguous use of the term 'authority' in here:
#In scraper sqlite stores 'authority' is the name of the store and the associated UKPlanning scraper (no spaces in this name)
#In the API 'authority' is the name of the associated planning areas (can have spaces)
#See 'sareas.py' for a map between the two sides

TARGETS = {
    'DEVPLANIT': 'postgresql://postgres:poohbear123@dev.planit.org.uk:5432/planit',
    'LOCAL': 'postgresql://postgres:poohbear123@localhost:5432/planit',
    'SQLITE': '.pg.sqlite'
    }
IN_TABLE = 'data'
OUT_TABLE = 'plan_applics'
PC_TABLE = 'postcodes'
AVAILABLE_SECS = 300.0
BATCH_SIZE = 5000
TOP_FIELDS = {
    'name': 'name', # key=source field, value=target field
    'uid': 'uid', 
    'url': 'url', 
    'source_url': 'source_url', 
    'address': 'address', 
    'description': 'description', 
    'associated_id': 'associated_id', 
    'altid': 'altid', 
    'reference': 'reference', 
    'app_size': 'app_size', 
    'app_state': 'app_state', 
    'app_type': 'app_type',
    'start_date': 'start_date',
    'decided_date': 'decided_date',
    'last_scraped': 'last_scraped',
    'postcode': 'postcode',
    }
LINK_ENDPOINT = 'https://www.planit.org.uk/planapplic/%s/'
PCLOOKUPDB = 'LookupPostcodes.sqlite'

start_time = time.time()

store_choices = list(TARGETS.keys())

parser = argparse.ArgumentParser(description='Upload data from a scraper SQLITE database to a master Postgres store')
parser.add_argument("scrape_db", help="Path to the scraper database")
parser.add_argument("-u", "--upload_db", help="Target db store to upload to", choices=store_choices, default='LOCAL')
parser.add_argument("-t", "--time_secs", help="Time available for completion (secs)", default=AVAILABLE_SECS, type=int)
parser.add_argument("-r", "--recent_days", help="Restrict to recently scraped period only (days)", default=0, type=int)
parser.add_argument("-c", "--clear", help="Clear existing data before upload", action='store_true')
parser.add_argument("-q", "--quiet", help="Suppress printed output", action='store_true')
parser.add_argument("-v", "--verbose", help="Additional printed output", action='store_true')
args = parser.parse_args()

conditions = 'start_date is not null' # full applications not yet postprocessed 
if args.recent_days and args.recent_days > 0: # restrict to most recently scraped only
    cutoff_datetime = datetime.now() - timedelta(days=args.recent_days)
    conditions += " and date_scraped >= %s" % nquote(cutoff_datetime.isoformat())
#conditions += ' order by date_scraped desc nulls last' # returns with most recently scraped first and nulls last (Postgres considers nulls as large)
conditions += ' order by date_scraped desc' # returns with most recently scraped first and nulls last(mySQL/SQLITE consider nulls as small)
conditions += ' limit %d' % (BATCH_SIZE) 

base = os.path.basename(args.scrape_db)
scraper_name = os.path.splitext(base)[0] # scraper name assumed to be same as store name

instore = DBTruck(args.scrape_db, data_table = IN_TABLE)
if args.upload_db == 'SQLITE':
    target_db = os.path.dirname(args.scrape_db) + os.sep + scraper_name + TARGETS[args.upload_db]
else:
    target_db = TARGETS[args.upload_db]
outstore = DBTruck(target_db, data_table = OUT_TABLE)

total_secs = 0.0
offset = 0
total = instore.count(conditions = conditions)
tables = outstore.tables()

pcstore = None
pc_db = os.path.dirname(args.scrape_db) + os.sep + PCLOOKUPDB
if PC_TABLE in tables:
    pcstore = outstore
elif os.path.isfile(pc_db):
    pcstore = DBTruck(pc_db, data_table = PC_TABLE)
    
if not args.quiet:
    if args.verbose:
        print ('Started uploading %d rows from %s to %s' % (total, scraper_name, target_db))
    else:
        print (scraper_name, end=' ', flush=True)
    
if args.clear and OUT_TABLE in tables and SCRAPER_AREAS.get(scraper_name):
    aid = SCRAPER_AREAS[scraper_name][0]
    deleted = outstore.delete(conditions = 'authority_id = %d' % aid)
    if not args.quiet and args.verbose:
        print('Deleted %d rows from %s(%d)' % (deleted, scraper_name, aid))
        
pclookups = 0
    
while total_secs <= args.time_secs:

    bstart_time = time.time()

    batch_conditions = conditions + (' offset %d' % offset)
    applics = instore.select(conditions = batch_conditions)
    
    if not applics or len(applics) == 0: break

    secs_taken = time.time() - bstart_time
    bstart_time = time.time()
    if not args.quiet and args.verbose:
        print ('Retrieved %d applications in %.1f secs' % (len(applics), secs_taken))

    processed = []
    for r in applics:
        filt_r = postprocess.filtered_record(r) # removes bad values, renames date_scraped to last_scraped, checks number formats, removes bad altid/reference etc
        postprocess.process_full(filt_r) # (re)derives all derived fields, including name
        if filt_r.get('name'): # this is the key so has to exist
            data = {}
            #filt_r.pop('rowid', None) not required?
            name = filt_r['name']
            if name.endswith('/'):
                name = name + '*'
            data['link'] = LINK_ENDPOINT % name.replace('//', '/*')
            data['lat'] = None; data['lng'] = None
            if filt_r.get('lat') is not None and filt_r.get('lng') is not None:
                data['lat'] = filt_r['lat']; data['lng'] = filt_r['lng']
            elif pcstore and filt_r.get('postcode'):
                pcde_info = pcstore.select(table_name=PC_TABLE, conditions="name = %s" % nquote(filt_r['postcode']))
                if pcde_info and len(pcde_info) == 1 and pcde_info[0]['matched'] != 'No match':
                    data['lat'] = pcde_info[0]['lat']; data['lng'] = pcde_info[0]['lng']
                    pclookups += 1
            if filt_r.get('authority') and SCRAPER_AREAS.get(filt_r['authority']): 
                data['authority_id'] = SCRAPER_AREAS[filt_r['authority']][0]
                data['authority_name'] = SCRAPER_AREAS[filt_r['authority']][1]
            for k, v in TOP_FIELDS.items(): # set top level text fields
                data[v] = filt_r.get(k) # default set to None 
            if filt_r.get('docs_json'): # JSON list
                try:
                    data['docs'] = json.loads(filt_r['docs_json'])
                    if not filt_r.get('n_documents'):
                        filt_r['n_documents'] = len(data['docs'])
                    filt_r.pop('docs_json', None)
                except ValueError as e:
                    pass
            else:
                data['docs'] = None
            data['all_fields'] = filt_r
            processed.append(data)

    secs_taken = time.time() - bstart_time
    bstart_time = time.time()
    if not args.quiet and args.verbose:
        print ('Processed %d applications in %.1f secs' % (len(applics),secs_taken))

    if processed:
        if args.clear:
            outstore.insert(processed, commit=True) # if table already cleared, we can do insert (avoids implicit DELETE)
        else:
            outstore.save(processed, commit=True) # has additional DELETE to remove any entries with same keys
    
    secs_taken = time.time() - bstart_time
    if not args.quiet and args.verbose:
        print ('Saved %d applications in %.1f secs' % (len(applics), secs_taken))
    
    offset += len(applics)
    total_secs = time.time() - start_time
    
    if not args.quiet and not args.verbose:
        print('.', end='', flush=True) # progress indicator
        
if not args.quiet:
    rate = offset * 3.6 / total_secs # rate per hour for 1k applications
    if args.verbose:
        print('Finished uploading', end='')
    else:
        print(' -> uploaded', end='')
    if offset == total:
        print (' all %d applications (inc %d postcode lookups) in %.1fs (%.0fk p/h)' % (offset, pclookups, total_secs, rate))
    else:
        print (' %d of %d applications (inc %d postcode lookups) in %.1fs (%.0fk p/h)' % (offset, total, pclookups, total_secs, rate))

